﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookingBook.Models
{
    [Serializable]
    public class Recipe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgPath { get; set; }
        

        public Recipe(int id, string name, string description, string imgPath)
        {
            Id = id;
            Name = name;
            Description = description;
            ImgPath = imgPath;
        }
        public Recipe()
        {
            Id = 0;
            Name = "";
            Description = "";
            ImgPath = "";
        }
    }
}
