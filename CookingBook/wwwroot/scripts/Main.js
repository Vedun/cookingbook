﻿$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: '/dev/allrecipes',
        success: function (recipes) {
            for (var i = 0; i < recipes.length; i++) {
                $('<div>', { class: 'item', text: recipes[i].name, recipeId:recipes[i].id}).appendTo('#left-panel');
            }
        }
    });
    $('#content-details').attr('display', 'static').fadeIn(1000);

    $(document).on('click','.item', function () {
        var id = $(this).attr('recipeId');
        $.ajax({
            type: 'GET',
            url: '/dev/getdetails/' + id,
            success: function (recipe) {
                $('#details').html(recipe.description);
                $('#recipeImg').attr('src', recipe.imgPath).fadeIn(0);
            }
        });
    });
    
});
