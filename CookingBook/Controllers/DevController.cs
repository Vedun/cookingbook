﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using CookingBook.Models;

namespace CookingBook.controllers
{
    [Route("dev")]
    public class DevController : Controller
    {
        const string ConnectionString = @"Server=tcp:mycook.database.windows.net,1433;Initial Catalog=MyCook;Persist Security Info=False;User ID=Demo;Password=observer_1;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        private SqlConnection connection;
        private SqlCommand recipeById;

        private SqlCommand allRecipes;
        private SqlParameter recipeId;

        public DevController():base()
        {
            connection = new SqlConnection(ConnectionString);
            recipeId = new SqlParameter("@Id", System.Data.SqlDbType.Int);
            allRecipes = new SqlCommand(@"select * from recipes", connection);
            recipeById = new SqlCommand(@"select * from recipes where Id=@Id", connection);
            recipeById.Parameters.Add(recipeId);
        }

        [Route("index")]
        [Route("")]
        [Route("~/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("allrecipes")]
        public IActionResult AllRecipes()
        {
            var recipes = new List<Recipe>();

            connection.Open();
            

            SqlDataReader reader = allRecipes.ExecuteReader();

            while (reader.Read())
            {
                recipes.Add(new Recipe(reader.GetInt32(0), reader.GetString(1), reader.GetString(2),reader.GetString(3)));
            }

            reader.Close();
            connection.Close();

            return new JsonResult(recipes);
        }

        [Route("getdetails/{id}")]
        public IActionResult GetDetails(string id)
        {
            connection.Open();
            recipeId.Value = id;
            SqlDataReader reader = recipeById.ExecuteReader();
            reader.Read();

            var recipe = new Recipe(reader.GetInt32(0),reader.GetString(1),reader.GetString(2), reader.GetString(3));

            reader.Close();
            connection.Close();

            return new JsonResult(recipe);

        }
    }
}